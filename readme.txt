=== Matomo tracking, by Sergio Santos ===
Contributors: sergiorcs82
Donate link: https://www.sergiosantos.me/
Tags: matomo, piwik, analytics, tracking, code
Requires at least: 3.0
Tested up to: 5.1
Stable tag: 1.1.2
License: GPLv3 or later
License URI: https://www.gnu.org/licenses/gpl-3.0.html

"Matomo tracking, by Sergio Santos" is a simple plugin whose only purpose is to add the Matomo tracking code to your website.

== Description ==

"Matomo tracking, by Sergio Santos" is a simple plugin whose only purpose is to add the Matomo tracking code to your website.

This will not install Matomo, so you will need to do that yourself. For more information about Matomo (formerly known as Piwik), visit [matomo.org](https://matomo.org/).

== Installation ==

This section describes how to install the plugin and get it working.

1. Unzip "piwik-tracking-by-mente-binaria.zip" and upload the "piwik-tracking-by-mente-binaria" folder to your plugin directory (usually "/wp-content/plugins/").
2. Activate the plugin through the "Plugins" menu in WordPress.
3. Visit the "Matomo tracking" page in the "Settings" section and adjust the parameters. Don't forget to click "Save Changes" when you're done.

== Frequently Asked Questions ==

= Can i install this plugin on my commercial website? =

Yes! This plugin is free for all uses and distributed under the GPLv3 licence.

= There's a problem. Can you fix it? =

Perhaps. Please file a bug report [here](https://plugins.trac.wordpress.org/report) and i will look into it as soon as possible.

= You're giving away your work for free. What's the catch? =

There is no catch - really. There are no hidden links, no hacks, nothing... the plugin just does what it says and nothing more.
The reason why i'm giving you this plugin is because someone else worked to give us WordPress for free - i'm just giving something back.

= This plugin is pretty cool. How can i repay you? =

Thanks! If you'd like to give something back, visit [this page](https://codex.wordpress.org/Contributing_to_WordPress) to see how you can contribute to WordPress.
If you're keen on thanking me personally, you can visit [my page](https://www.sergiosantos.me/) to see how we can help each other.

== Screenshots ==

1. **The main settings page.** *It uses the same look as the native pages. Everything is as simple as possible and all options have a small description to aid the user.*

== Changelog ==

= 1.1.2 =
* Added option to choose between SetCustomVariable (old) and SetUserID (better) method for tracking usernames.
* Fixed bug in noscript tag code output.

= 1.1.1 =
* Added option to disable cookies.

= 1.1.0 =
* Renamed Piwik to Matomo.
* Implemented internal search tracking.
* Confirmed compatibility with WordPress 5.1 .

= 1.0.14 =
* Confirmed compatibility with WordPress 4.9 .
* Improved support for localization.

= 1.0.13 =
* Improved support for localization.

= 1.0.12 =
* Slightly improved SSL support in tracking code.

= 1.0.11 =
* Added support for heartbeat timer.
* Improved plugin internal structure.
* Slightly improved noscript image styling.

= 1.0.10 =
* Confirmed compatibility with WordPress 4.8.1 .
* Moved JavaScript code to head.
* Updated graphics.

= 1.0.9 =
* Confirmed compatibility with WordPress 4.7.3 .
* Updated information.

= 1.0.8 =
* Fixed settings loss when upgrading to 1.0.7 .

= 1.0.7 =
* Reviewed code and screenshots.
* Replaced old identity (Mente Binaria) with new one (Sergio Santos).
* Added Portuguese translation.

= 1.0.6 =
* Ports are now accepted in address.

= 1.0.5 =
* Fixed minor bug in settings page.

= 1.0.4 =
* Added option to log user names (thanks cwerner1111).

= 1.0.3 =
* Fixed bug in script output (thanks cwerner1111).

= 1.0.2 =
* Improved readme.txt.
* Added banners.

= 1.0.1 =
* Improved the code structure.

= 1.0.0 =
* Initial version.

== Upgrade Notice ==

= 1.0.8 =
This fixes the settings loss bug introduced by 1.0.7 .

= 1.0.7 =
Translated to Portuguese.

= 1.0.4 =
Support for logging user names was added.

= 1.0.3 =
This corrects a bug in the script output that might cause the tracking to malfunction. You should upgrade as soon as possible.

= 1.0.2 =
There were no changes, i only improved the ReadMe file.

= 1.0.1 =
This version does basically the same as the previous one, i just did some minor improvements to the structure of the code.
